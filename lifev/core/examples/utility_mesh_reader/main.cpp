/*
 * test_genAlpha.cpp
 *
 *  Created on: Jul 27, 2010
 *      Author: uvilla
 */

#include <Epetra_ConfigDefs.h>
#include <Epetra_Comm.h>

#include <lifev/core/LifeV.hpp>
#include <Teuchos_YamlParameterListHelpers.hpp>
#include <Teuchos_XMLParameterListHelpers.hpp>
#include <lifev/core/mesh/RegionMesh.hpp>
#include <lifev/core/mesh/MeshLoadingUtility.hpp>

using namespace LifeV;

// Do not edit
int main (int argc, char** argv)
{
    using namespace LifeV;

    if (argc != 2)
	{
		std::cout << "Usge: xyfem yaml/xml file" << std::endl;
		return -1;
	}

#ifdef HAVE_MPI
    MPI_Init (&argc, &argv);
    std::cout << "MPI Initialization\n";
#endif

#ifdef EPETRA_MPI
    std::shared_ptr<Epetra_Comm> comm (new Epetra_MpiComm (MPI_COMM_WORLD) );
#else
    std::shared_ptr<Epetra_Comm> comm (new Epetra_SerialComm() );
#endif

    std::string dataFileName;
    Teuchos::RCP<Teuchos::ParameterList> appParams = Teuchos::createParameterList("XYFEM Parameters");
    auto p = strrchr(argv[1],'.');
    if( p==NULL ) {
        std::cout << "Input file unknown! Please use yaml or xml file." << std::endl;
	    return -1;
    }
	auto const input_extension = std::string( p );
    if (input_extension == ".yaml" || input_extension == ".yml") {
        Teuchos::updateParametersFromYamlFile(
            std::string(argv[1]), appParams.ptr());
    } else if (input_extension == ".xml" ) {
        Teuchos::updateParametersFromXmlFile(
            std::string(argv[1]), appParams.ptr());
    } else {
        std::cout << "Input file unknown! Please use yaml or xml file." << std::endl;
		return -1;
    }
    appParams->print(std::cout,
         Teuchos::ParameterList::PrintOptions().showDoc(true).indent(2).showTypes(true));

    if ( comm -> MyPID() == 0 )
    {
        std::cout << "\n\nReading and partitioning the cube mesh without saving the global mesh: ... \n";
    }

    auto mpl = Teuchos::sublist(appParams, "Mesh", true);
    auto meshName = mpl->get<std::string>("mesh_file");
    std::cout << "Mesh name:" << meshName << std::endl;
    auto meshPath =  mpl->get<std::string>("mesh_dir");
    auto meshOrder =  mpl->get<std::string>("mesh_order");
    bool isPartitioned = false;

    {
        //Create the mesh data and read and partitioned the mesh
        std::shared_ptr< RegionMesh <LinearTetra> > meshPtr ( new RegionMesh <LinearTetra> ( comm ) );
        MeshUtility::loadMesh ( meshPtr, meshName, meshPath, isPartitioned, meshOrder );
        if ( comm -> MyPID() == 0 )
        {
            std::cout << "... DONE! ";
        }
    }

    {
        //create the mesh data and read and save both the global mesh and the partitioned mesh
        if ( comm -> MyPID() == 0 )
        {
            std::cout << "\n\nReading and partitioning the cube mesh saving the global mesh: ... \n";
        }
        std::shared_ptr< RegionMesh <LinearTetra> > meshFullPtr ( new RegionMesh <LinearTetra> ( comm ) );
        std::shared_ptr< RegionMesh <LinearTetra> > meshLocalPtr ( new RegionMesh <LinearTetra> ( comm ) );
        MeshUtility::loadMesh ( meshLocalPtr, meshFullPtr, meshName, meshPath, isPartitioned, "P1" );
        if ( comm -> MyPID() == 0 )
        {
            std::cout << "... DONE! ";
        }
    }

    {
        //create a 3D structured mesh
        if ( comm -> MyPID() == 0 )
        {
            std::cout << "\n\nCreating a structured mesh without saving the full mesh: ... \n";
        }
        std::shared_ptr< RegionMesh <LinearTetra> > meshStructPtr ( new RegionMesh <LinearTetra> ( comm ) );
        std::vector<Real> l (3, 1.0);
        std::vector<Real> t (3, 0.0);
        MeshUtility::loadStructuredMesh  ( meshStructPtr,
                                           1,
                                           std::vector<UInt> (3, 5),
                                           true,
                                           l,
                                           t );


        if ( comm -> MyPID() == 0 )
        {
            std::cout << "... DONE!\n\n ";
        }
    }

    comm.reset();

#ifdef HAVE_MPI
    MPI_Finalize();
    std::cout << "MPI Finalization \n";
#endif

    return 0;
}
