LifeV (to be read _Life Five_) is an open source library for the numerical solution of
partial differential equations.

Distributed under LGPL by CMCS – EPFL, E(CM)2 – Emory, MOX – Polimi, REO, ESTIME– INRIA.

[Copyright](https://bitbucket.org/hillyuan/lifev/wiki/Copyright.md)

[LifeV web site](http://www.lifev.org)

[Development pages](https://bitbucket.org/hillyuan/lifev/wiki/Home)

[Tutorial](https://bitbucket.org/hillyuan/lifev/wiki/tutorial)

----------------------------------------------------------------------------
Contact: https://groups.google.com/forum/?fromgroups#!forum/lifev-user
